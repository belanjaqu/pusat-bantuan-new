<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Bootstrap Admin Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../css/plugins/morris.css" rel="stylesheet">

    <link href="../css/gaya.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="../../images/favicon.png"/>

</head>

<?php
session_start();
date_default_timezone_set('Asia/Jakarta');
require "../../proses/koneksi.php";

  if(empty($_SESSION['email'])){
	   header("location:../login.php");
	}
  $email = $_SESSION['email'];
	// $query = mysqli_query($connect, "SELECT * FROM admin where email = '$email' ");
    $query = "SELECT * FROM admin where email = :e";
    $stmt = $connect->prepare($query);
    $stmt->execute(['e' => $email]);
	// $admin = mysqli_fetch_assoc($query);
    $admin = $stmt->fetch();

?>
