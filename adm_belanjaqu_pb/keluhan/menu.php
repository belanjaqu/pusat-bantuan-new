<style type="text/css">
@media screen and (max-width: 767px){.navbar-collapse {background: #3d3d3d;}}
.ikon_pilari{padding:2px 3px 4px 3px}.eusi-tabel{font-size:13px}
</style>

<?php
echo '<!DOCTYPE html>',
'<html lang="en">';

include "head.php";

echo '<body>';

?>
    <div id="wrapper">
      <!-- Navigation -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">Pusat Bantuan Belanjaqu</a>
          </div>
          <!-- Top Menu Items -->
          <ul class="nav navbar-right top-nav">
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $admin['nama'];?> <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                      <li>
                          <a href="#"><i class="fa fa-fw fa-envelope"></i> <?php echo $admin['email'];?></a>
                      </li>
                      <li>
                          <a href="#"><i class="fa fa-fw fa-black-tie"></i> <?php echo $admin['jabatan'];?></a>
                      </li>
                      <li>
                          <a href="#"><i class="fa fa-fw fa-unlock-alt"></i> <?php echo $admin['hak'];?></a>
                      </li>
                      <li class="divider"></li>
                      <li>
                          <a href="../keluar.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                      </li>
                  </ul>
              </li>
          </ul>


            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="../index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li class="active">
                        <a style="cursor:pointer;" id="flip"><i class="fa fa-fw fa-table"></i> Keluhan <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="panel">
                          <li>
                              <a href="all.php?status=all">All</a>
                          </li>
                          <li>
                              <a href="pembayaran.php?status=all">Pembayaran</a>
                          </li>
                          <li>
                              <a href="pengiriman.php?status=all">Pengiriman</a>
                          </li>
                          <li>
                              <a href="produk.php?status=all">Produk</a>
                          </li>
                          <li>
                              <a href="pembelian.php?status=all">Pembelian</a>
                          </li>
                          <li>
                              <a href="akun.php?status=all">Akun</a>
                          </li>
                          <li>
                              <a href="fitur.php?status=all">Fitur Belanjaqu</a>
                          </li>
                        </ul>
                    </li>

                    <li>
                        <a href="../admin.php"><i class="fa fa-fw fa-user"></i> Admin</a>
                    </li>
                </ul>
            </div>
          </nav>

          <!-- jQuery -->
          <script src="js/jquery.js"></script>

          <!-- Bootstrap Core JavaScript -->
          <script src="../js/bootstrap.min.js"></script>

          <!-- Morris Charts JavaScript -->
          <script src="../js/plugins/morris/raphael.min.js"></script>
          <script src="../js/plugins/morris/morris.min.js"></script>
          <script src="../js/plugins/morris/morris-data.js"></script>

          <script>
            $("#flip").click(function(){
            $("#panel").slideToggle("slow");
            });
          </script>
