<?php
require "../../proses/koneksi.php";
$time = date("Y-m-d H:i:s");
if($_GET['id_all']){
	$status = $_GET['status'];
	$halaman = $_GET['halaman'];
		$id = $_GET['id_all'];
		// $query = mysqli_query($connect, "DELETE from keluhan where id_ticket = '$id'");
		// $query = mysqli_query($connect, "UPDATE keluhan SET deleted_at = '$time' WHERE id_ticket = '$id'");
		$query = "UPDATE keluhan SET deleted_at = :timer WHERE id_ticket = :id";
    $stmt = $connect->prepare($query);                                  
    $stmt->bindParam(':timer', $time);       
    $stmt->bindParam(':id', $id);      
		if($stmt->execute()){
    		echo "<script>alert('Berhasil')</script>";
    		// $utime = date('Y-m-d H:i:s');
    		// $que = mysqli_query($connect, "INSERT INTO activities VALUES('Hapus ID All Oleh Admin', '$utime', '$utime')");
				if(empty($halaman)){
					header("location:all.php?status=" . $status);
				} else {
					header("location:all.php?status=" . $status . "&halaman=" . $halaman);
				}
		}else{
    		echo "<script>alert('Gagal')</script>";
  		}
	} else if($_GET['id_akun']){
			$id = $_GET['id_akun'];
			// $query = mysqli_query($connect, "DELETE from keluhan where id_ticket = '$id'");
			// $query = mysqli_query($connect, "UPDATE keluhan SET deleted_at = '$time' WHERE id_ticket = '$id'");
			$query = "UPDATE keluhan SET deleted_at = :timer WHERE id_ticket = :id";
	    $stmt = $connect->prepare($query);                                  
	    $stmt->bindParam(':timer', $time);       
	    $stmt->bindParam(':id', $id);      
			if($stmt->execute()){
	    		echo "<script>alert('Berhasil')</script>";
	    		// $utime = date('Y-m-d H:i:s');
    			// $que = mysqli_query($connect, "INSERT INTO activities VALUES('Hapus ID Akun Oleh Admin', '$utime', '$utime')");
					if(empty($halaman)){
						header("location:akun.php?status=" . $status);
					} else {
						header("location:akun.php?status=" . $status . "&halaman=" . $halaman);
					}
			}else{
	    		echo "<script>alert('Gagal')</script>";
	  		}
		} else if($_GET['id_fitur']){
				$id = $_GET['id_fitur'];
				// $query = mysqli_query($connect, "DELETE from keluhan where id_ticket = '$id'");
				// $query = mysqli_query($connect, "UPDATE keluhan SET deleted_at = '$time' WHERE id_ticket = '$id'");
				$query = "UPDATE keluhan SET deleted_at = :timer WHERE id_ticket = :id";
		    $stmt = $connect->prepare($query);                                  
		    $stmt->bindParam(':timer', $time);       
		    $stmt->bindParam(':id', $id);      
				if($stmt->execute()){
		    		echo "<script>alert('Berhasil')</script>";
		    		// $utime = date('Y-m-d H:i:s');
    				// $que = mysqli_query($connect, "INSERT INTO activities VALUES('Hapus ID Fitur Oleh Admin', '$utime', '$utime')");
						if(empty($halaman)){
							header("location:fitur.php?status=" . $status);
						} else {
							header("location:fitur.php?status=" . $status . "&halaman=" . $halaman);
						}
				}else{
		    		echo "<script>alert('Gagal')</script>";
		  		}
			} else if($_GET['id_pembayaran']){
					$id = $_GET['id_pembayaran'];
					// $query = mysqli_query($connect, "DELETE from keluhan where id_ticket = '$id'");
					// $query = mysqli_query($connect, "UPDATE keluhan SET deleted_at = '$time' WHERE id_ticket = '$id'");
					$query = "UPDATE keluhan SET deleted_at = :timer WHERE id_ticket = :id";
			    $stmt = $connect->prepare($query);                                  
			    $stmt->bindParam(':timer', $time);       
			    $stmt->bindParam(':id', $id);      
					if($stmt->execute()){
			    		echo "<script>alert('Berhasil')</script>";
			    		// $utime = date('Y-m-d H:i:s');
    					// $que = mysqli_query($connect, "INSERT INTO activities VALUES('Hapus ID Pembayaran Oleh Admin', '$utime', '$utime')");
							if(empty($halaman)){
								header("location:pembayaran.php?status=" . $status);
							} else {
								header("location:pembayaran.php?status=" . $status . "&halaman=" . $halaman);
							}
					}else{
			    		echo "<script>alert('Gagal')</script>";
			  		}
				} else if($_GET['id_pengiriman']){
						$id = $_GET['id_pengiriman'];
						// $query = mysqli_query($connect, "DELETE from keluhan where id_ticket = '$id'");
						// $query = mysqli_query($connect, "UPDATE keluhan SET deleted_at = '$time' WHERE id_ticket = '$id'");
						$query = "UPDATE keluhan SET deleted_at = :timer WHERE id_ticket = :id";
				    $stmt = $connect->prepare($query);                                  
				    $stmt->bindParam(':timer', $time);       
				    $stmt->bindParam(':id', $id);      
						if($stmt->execute()){
				    		echo "<script>alert('Berhasil')</script>";
				    		// $utime = date('Y-m-d H:i:s');
    						// $que = mysqli_query($connect, "INSERT INTO activities VALUES('Hapus ID Pengiriman Oleh Admin', '$utime', '$utime')");
								if(empty($halaman)){
									header("location:pengiriman.php?status=" . $status);
								} else {
									header("location:pengiriman.php?status=" . $status . "&halaman=" . $halaman);
								}
						}else{
				    		echo "<script>alert('Gagal')</script>";
				  		}
					} else if($_GET['id_produk']){
							$id = $_GET['id_produk'];
							// $query = mysqli_query($connect, "DELETE from keluhan where id_ticket = '$id'");
							// $query = mysqli_query($connect, "UPDATE keluhan SET deleted_at = '$time' WHERE id_ticket = '$id'");
							$query = "UPDATE keluhan SET deleted_at = :timer WHERE id_ticket = :id";
					    $stmt = $connect->prepare($query);                                  
					    $stmt->bindParam(':timer', $time);       
					    $stmt->bindParam(':id', $id);      
							if($stmt->execute()){
					    		echo "<script>alert('Berhasil')</script>";
					    		// $utime = date('Y-m-d H:i:s');
    							// $que = mysqli_query($connect, "INSERT INTO activities VALUES('Hapus ID Produk Oleh Admin', '$utime', '$utime')");
									if(empty($halaman)){
										header("location:produk.php?status=" . $status);
									} else {
										header("location:produk.php?status=" . $status . "&halaman=" . $halaman);
									}
							}else{
					    		echo "<script>alert('Gagal')</script>";
					  		}
						} else if($_GET['id_Pembelian']){
								$id = $_GET['id_Pembelian'];
								// $query = mysqli_query($connect, "DELETE from keluhan where id_ticket = '$id'");
								// $query = mysqli_query($connect, "UPDATE keluhan SET deleted_at = '$time' WHERE id_ticket = '$id'");
								$query = "UPDATE keluhan SET deleted_at = :timer WHERE id_ticket = :id";
						    $stmt = $connect->prepare($query);                                  
						    $stmt->bindParam(':timer', $time);       
						    $stmt->bindParam(':id', $id);      
								if($stmt->execute()){
						    		echo "<script>alert('Berhasil')</script>";
						    		// $utime = date('Y-m-d H:i:s');
    								// $que = mysqli_query($connect, "INSERT INTO activities VALUES('Hapus ID Pembelian Oleh Admin', '$utime', '$utime')");
										if(empty($halaman)){
											header("location:pembelian.php?status=" . $status);
										} else {
											header("location:pembelian.php?status=" . $status . "&halaman=" . $halaman);
										}
								}else{
						    		echo "<script>alert('Gagal')</script>";
						  		}
							} else if($_GET['id_admin']){
									$id = $_GET['id_admin'];
									// $query = mysqli_query($connect, "DELETE from admin where id = '$id'");
									// $query = mysqli_query($connect, "UPDATE keluhan SET deleted_at = '$time' WHERE id_ticket = '$id'");
									$query = "UPDATE admin SET deleted_at = :timer WHERE id_ticket = :id";
							    $stmt = $connect->prepare($query);                                  
							    $stmt->bindParam(':timer', $time);       
							    $stmt->bindParam(':id', $id);      
									if($stmt->execute()){
							    		echo "<script>alert('Berhasil')</script>";
							    		// $utime = date('Y-m-d H:i:s');
    									// $que = mysqli_query($connect, "INSERT INTO activities VALUES('Hapus ID Admin Oleh Admin', '$utime', '$utime')");
											header("location:../admin.php");
									}else{
							    		echo "<script>alert('Gagal')</script>";
							  		}
								}
?>
