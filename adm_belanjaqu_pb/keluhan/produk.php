<?php
  include "menu.php";

  if(@$_GET['id']){
  		$id = $_GET['id'];
      $time = date('Y-m-d H:i:s');
  		// $query = mysqli_query($connect, "DELETE from keluhan where id_ticket = '$id'");
      // $query = mysqli_query($connect, "UPDATE keluhan SET deleted_at = '$time' WHERE id_ticket = '$id'");
      $query = "UPDATE keluhan SET deleted_at = :timer WHERE id_ticket = :id";
      $stmt = $connect->prepare($query);                                  
      $stmt->bindParam(':timer', $time);       
      $stmt->bindParam(':id', $id);      
      if($stmt->execute()){
      		echo "<script>alert('Berhasil')</script>";
  				header("location:../keluhan.php?kategori=all");
  		}else{
      		echo "<script>alert('Gagal')</script>";
    		}
  	}
?>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12"><br/>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-table"></i> Keluhan
                    </li>
                    <li class="active">
                      Produk
                    </li>
                </ol>
            </div>
        </div>

        <div class="row"><br/>
          <div class="col-lg-6">
            <a href="../export_produk.php" target="_blank">Download Excel</a><br><br>
            <form role="search" action="../search.php" method="GET">
              <div class="input-group">
                  <?php
                    if(!empty($_GET['cari'])){
                      $y = $_GET['cari'];
                      $z = str_replace("-", " ", "$y");
                      echo '<input value="'.$z.'" type="text" class="form-control" placeholder="Pencarian..." name="cari" required />';
                    }else{
                      echo '<input type="text" class="form-control" placeholder="Pencarian..." name="cari" required />';
                    }
                    ?>
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <i class="glyphicon glyphicon-search ikon_pilari"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>

          <div class="col-lg-6">
            <div class="btn-group">
              <a href="?status=all" class="btn btn-default">All</a>
              <a href="?status=read" class="btn btn-default">Read</a>
              <a href="?status=unread" class="btn btn-default">Unread</a>
              <a href="?status=pesan_baru" class="btn btn-default">Pesan Baru</a>
            </div>
            <?php
              if(@$_GET["status"] == 'all'){
                echo 'Status = <span class="label label-lg label-info">All</span>';
              } else if(@$_GET["status"] == 'read'){
                echo 'Status = <span class="label label-lg label-success">Read</span>';
              } else if(@$_GET["status"] == 'unread'){
                echo 'Status = <span class="label label-lg label-warning">Unread</span>';
              } else if(@$_GET["status"] == 'pesan_baru'){
                echo 'Status = <span class="label label-lg label-success">Pesan Baru</span>';
              }
            ?>
            <br>
            <br>
            <br>
          </div>

        </div>
        <!-- /.row -->

        <div class="row">
              <?php
                if (@$_GET["status"] == 'all'){
              ?>
              <div class="col-lg-12">
                <div class="table-responsive table-bordered">
                  <?php
                  $batas = 15;
                  $halaman = @$_GET['halaman'];
                  if(empty($halaman)){
                     $posisi = 0;
                     $halaman = 1;
                    }else{
                       $posisi = ($halaman - 1) * $batas;
                    }

                         // $query = mysqli_query($connect, "SELECT * FROM keluhan WHERE kategori = 'Produk' AND deleted_at IS NULL ORDER BY created_at DESC LIMIT $posisi,$batas");
                         $query = "SELECT * FROM keluhan WHERE kategori = :a AND deleted_at IS :b ORDER BY created_at DESC LIMIT $posisi,$batas";
                         $stmt = $connect->prepare($query);
                         $stmt->execute(['a' => 'Produk', 'b' => NULL]);
                         $total = $stmt->rowCount();
                         // $total = mysqli_num_rows($query);
                         ?>
                         <table class="table table-hover table-striped eusi-tabel">
                           <thead>
                               <tr>
                                   <th>ID Ticket</th>
                                   <th>Waktu</th>
                                   <th>Pengirim</th>
                                   <th>Email</th>
                                   <th>Keluhan</th>
                                   <th>Status</th>
                                   <th>Pesan Baru</th>
                                   <th>Aksi</th>
                               </tr>
                           </thead>
                           <tbody>
                             <?php
                                    $no = 1+$posisi;
                                    while($data1 = $stmt->fetch()){
                                 ?>
                               <tr>
                                   <td><?php echo $data1['id_ticket'];?></td>
                                   <td><?php echo $data1['waktu'];?></td>
                                   <td><?php echo $data1['pengirim'];?></td>
                                   <td><?php echo $data1['email'];?></td>
                                   <td><?php echo $data1['keluhan'];?></td>
                                   <td>
                                     <?php
                                   if($data1["status"]=='Read'){
                                     ?>
                                     <span class="label label-lg label-success"><?php echo $data1['status'];?></span>
                                     <?php
                                   }else if($data1["status"]=='Unread'){
                                     ?>
                                     <span class="label label-lg label-warning"><?php echo $data1['status'];?></span>
                                     <?php
                                   }
                                     ?>
                                   </td>
                                   <td><?php echo $data1['pesan_baru'];?></td>
                                   <td><a href="../detail_keluhan.php?id=<?php echo $data1['id_ticket'];?>" class="btn btn-xs btn-primary btn-sm">Detail</a> <a href="hapus.php?id_produk=<?php echo $data1['id_ticket'];?>&status=<?php echo @$_GET["status"];?>&halaman=<?php echo @$_GET["halaman"];?>" class="btn btn-xs btn-danger btn-sm">Delete</a></td>
                               </tr>
                               <?php
                               $no++;
                                 }
                               ?>
                           </tbody>
                         </table>
                    <div class="col-md-12">
                    <div class="blog-pagination">
                      <center>
                      <ul class="pagination">
                        <?php
                        // $paging2 = mysqli_query($connect,"SELECT * FROM keluhan WHERE kategori = 'Produk' AND deleted_at IS NULL ORDER BY created_at DESC");
                        $paging2 = "SELECT * FROM keluhan WHERE kategori = :a AND deleted_at IS :b ORDER BY created_at DESC";
                        $stmt = $connect->prepare($paging2);
                        $stmt->execute(['a' => 'Produk', 'b' => NULL]);
                        $jmldata = $stmt->rowCount();
                        // $jmldata = mysqli_num_rows($paging2);
                        $jmlhalaman = ceil($jmldata/$batas);
                        for($i=1; $i<=$jmlhalaman; $i++){
                          if($i != $halaman){
                            ?>
                            <li><a href="?status=all&halaman=<?php echo $i?>" class="btn-xs btn-primary"><?php echo $i?></a></li>
                            <?php
                          }else{
                            ?>
                            <li class="active"><a style="padding:4px 10px" href="?kategori=all&halaman=<?php echo $i?>"><?php echo $i?></a></li>
                            <?php
                          }
                        }
                        ?>
                      </ul>
                    </center>
                    </div>
                  </div>
                </div>
              </div>
              <?php
            }else if (@$_GET["status"] == 'read'){
          ?>
          <div class="col-lg-12">
            <div class="table-responsive table-bordered">
              <?php
              $batas = 15;
              $halaman = @$_GET['halaman'];
              if(empty($halaman)){
                 $posisi = 0;
                 $halaman = 1;
                }else{
                   $posisi = ($halaman - 1) * $batas;
                }

                     // $query = mysqli_query($connect, "SELECT * FROM keluhan WHERE kategori = 'Produk' AND status = 'read' AND deleted_at IS NULL ORDER BY created_at DESC LIMIT $posisi,$batas");
                     $query = "SELECT * FROM keluhan WHERE kategori = :a AND status = :b AND deleted_at IS :c ORDER BY created_at DESC LIMIT $posisi,$batas";
                     $stmt = $connect->prepare($query);
                     $stmt->execute(['a' => 'Produk', 'b' => 'read', 'c' => NULL]);
                     $total = $stmt->rowCount();
                     // $total = mysqli_num_rows($query);
                     ?>
                     <table class="table table-hover table-striped eusi-tabel">
                       <thead>
                           <tr>
                               <th>ID Ticket</th>
                               <th>Waktu</th>
                               <th>Pengirim</th>
                               <th>Email</th>
                               <th>Keluhan</th>
                               <th>Status</th>
                               <th>Pesan Baru</th>
                               <th>Aksi</th>
                           </tr>
                       </thead>
                       <tbody>
                         <?php
                                $no = 1+$posisi;
                                while($data1 = $stmt->fetch()){
                             ?>
                           <tr>
                               <td><?php echo $data1['id_ticket'];?></td>
                               <td><?php echo $data1['waktu'];?></td>
                               <td><?php echo $data1['pengirim'];?></td>
                               <td><?php echo $data1['email'];?></td>
                               <td><?php echo $data1['keluhan'];?></td>
                               <td>
                                 <?php
                               if($data1["status"]=='Read'){
                                 ?>
                                 <span class="label label-lg label-success"><?php echo $data1['status'];?></span>
                                 <?php
                               }else if($data1["status"]=='Unread'){
                                 ?>
                                 <span class="label label-lg label-warning"><?php echo $data1['status'];?></span>
                                 <?php
                               }
                                 ?>
                               </td>
                               <td><?php echo $data1['pesan_baru'];?></td>
                               <td><a href="../detail_keluhan.php?id=<?php echo $data1['id_ticket'];?>" class="btn btn-xs btn-primary btn-sm">Detail</a> <a href="hapus.php?id_produk=<?php echo $data1['id_ticket'];?>&status=<?php echo @$_GET["status"];?>&halaman=<?php echo @$_GET["halaman"];?>" class="btn btn-xs btn-danger btn-sm">Delete</a></td>
                           </tr>
                           <?php
                           $no++;
                             }
                           ?>
                       </tbody>
                     </table>
                <div class="col-md-12">
                <div class="blog-pagination">
                  <center>
                  <ul class="pagination">
                    <?php
                    // $paging2 = mysqli_query($connect,"SELECT * FROM keluhan WHERE kategori = 'Produk' AND status = 'read' AND deleted_at IS NULL ORDER BY created_at DESC");
                    $paging2 = "SELECT * FROM keluhan WHERE kategori = :a AND status = :b AND deleted_at IS :c ORDER BY created_at DESC";
                    $stmt = $connect->prepare($paging2);
                    $stmt->execute(['a' => 'Produk', 'b' => 'read', 'c' => NULL]);
                    $jmldata = $stmt->rowCount();
                    // $jmldata = mysqli_num_rows($paging2);
                    $jmlhalaman = ceil($jmldata/$batas);
                    for($i=1; $i<=$jmlhalaman; $i++){
                      if($i != $halaman){
                        ?>
                        <li><a href="?status=read&halaman=<?php echo $i?>" class="btn-xs btn-primary"><?php echo $i?></a></li>
                        <?php
                      }else{
                        ?>
                        <li class="active"><a style="padding:4px 10px" href="?kategori=all&halaman=<?php echo $i?>"><?php echo $i?></a></li>
                        <?php
                      }
                    }
                    ?>
                  </ul>
                </center>
                </div>
              </div>
            </div>
          </div>
          <?php
        }else if (@$_GET["status"] == 'unread'){
      ?>
      <div class="col-lg-12">
        <div class="table-responsive table-bordered">
          <?php
          $batas = 15;
          $halaman = @$_GET['halaman'];
          if(empty($halaman)){
             $posisi = 0;
             $halaman = 1;
            }else{
               $posisi = ($halaman - 1) * $batas;
            }

                 // $query = mysqli_query($connect, "SELECT * FROM keluhan WHERE kategori = 'Produk' AND status = 'unread' AND deleted_at IS NULL ORDER BY created_at DESC LIMIT $posisi,$batas");
                 $query = "SELECT * FROM keluhan WHERE kategori = :a AND status = :b AND deleted_at IS :c ORDER BY created_at DESC LIMIT $posisi,$batas";
                 $stmt = $connect->prepare($query);
                 $stmt->execute(['a' => 'Produk', 'b' => 'unread', 'c' => NULL]);
                 $total = $stmt->rowCount();
                 // $total = mysqli_num_rows($query);
                 ?>
                 <table class="table table-hover table-striped eusi-tabel">
                   <thead>
                       <tr>
                           <th>ID Ticket</th>
                           <th>Waktu</th>
                           <th>Pengirim</th>
                           <th>Email</th>
                           <th>Keluhan</th>
                           <th>Status</th>
                           <th>Pesan Baru</th>
                           <th>Aksi</th>
                       </tr>
                   </thead>
                   <tbody>
                     <?php
                            $no = 1+$posisi;
                            while($data1 = $stmt->fetch()){
                         ?>
                       <tr>
                           <td><?php echo $data1['id_ticket'];?></td>
                           <td><?php echo $data1['waktu'];?></td>
                           <td><?php echo $data1['pengirim'];?></td>
                           <td><?php echo $data1['email'];?></td>
                           <td><?php echo $data1['keluhan'];?></td>
                           <td>
                             <?php
                           if($data1["status"]=='Read'){
                             ?>
                             <span class="label label-lg label-success"><?php echo $data1['status'];?></span>
                             <?php
                           }else if($data1["status"]=='Unread'){
                             ?>
                             <span class="label label-lg label-warning"><?php echo $data1['status'];?></span>
                             <?php
                           }
                             ?>
                           </td>
                           <td><?php echo $data1['pesan_baru'];?></td>
                           <td><a href="../detail_keluhan.php?id=<?php echo $data1['id_ticket'];?>" class="btn btn-xs btn-primary btn-sm">Detail</a> <a href="hapus.php?id_produk=<?php echo $data1['id_ticket'];?>&status=<?php echo @$_GET["status"];?>&halaman=<?php echo @$_GET["halaman"];?>" class="btn btn-xs btn-danger btn-sm">Delete</a></td>
                       </tr>
                       <?php
                       $no++;
                         }
                       ?>
                   </tbody>
                 </table>
            <div class="col-md-12">
            <div class="blog-pagination">
              <center>
              <ul class="pagination">
                <?php
                // $paging2 = mysqli_query($connect,"SELECT * FROM keluhan WHERE kategori = 'Produk' AND status = 'unread' AND deleted_at IS NULL ORDER BY created_at DESC");
                $paging2 = "SELECT * FROM keluhan WHERE kategori = :a AND status = :b AND deleted_at IS :c ORDER BY created_at DESC";
                $stmt = $connect->prepare($paging2);
                $stmt->execute(['a' => 'Produk', 'b' => 'unread', 'c' => NULL]);
                $jmldata = $stmt->rowCount();
                // $jmldata = mysqli_num_rows($paging2);
                $jmlhalaman = ceil($jmldata/$batas);
                for($i=1; $i<=$jmlhalaman; $i++){
                  if($i != $halaman){
                    ?>
                    <li><a href="?status=unread&halaman=<?php echo $i?>" class="btn-xs btn-primary"><?php echo $i?></a></li>
                    <?php
                  }else{
                    ?>
                    <li class="active"><a style="padding:4px 10px" href="?kategori=all&halaman=<?php echo $i?>"><?php echo $i?></a></li>
                    <?php
                  }
                }
                ?>
              </ul>
            </center>
            </div>
          </div>
        </div>
      </div>
      <?php
    }else if (@$_GET["status"] == 'pesan_baru'){
  ?>
  <div class="col-lg-12">
    <div class="table-responsive table-bordered">
      <?php
      $batas = 15;
      $halaman = @$_GET['halaman'];
      if(empty($halaman)){
         $posisi = 0;
         $halaman = 1;
        }else{
           $posisi = ($halaman - 1) * $batas;
        }

             // $query = mysqli_query($connect, "SELECT * FROM keluhan WHERE kategori = 'produk' AND pesan_baru = 'Ada' AND deleted_at IS NULL ORDER BY created_at DESC LIMIT $posisi,$batas");
             $query = "SELECT * FROM keluhan WHERE kategori = :a AND pesan_baru = :b AND deleted_at IS :c ORDER BY created_at DESC LIMIT $posisi,$batas";
             $stmt = $connect->prepare($query);
             $stmt->execute(['a' => 'Produk', 'b' => 'Ada', 'c' => NULL]);
             $total = $stmt->rowCount();
             // $total = mysqli_num_rows($query);
             ?>
             <table class="table table-hover table-striped eusi-tabel">
               <thead>
                   <tr>
                       <th>ID Ticket</th>
                       <th>Waktu</th>
                       <th>Pengirim</th>
                       <th>Email</th>
                       <th>Keluhan</th>
                       <th>Status</th>
                       <th>Pesan Baru</th>
                       <th>Aksi</th>
                   </tr>
               </thead>
               <tbody>
                 <?php
                        $no = 1+$posisi;
                        while($data1 = $stmt->fetch()){
                     ?>
                   <tr>
                       <td><?php echo $data1['id_ticket'];?></td>
                       <td><?php echo $data1['waktu'];?></td>
                       <td><?php echo $data1['pengirim'];?></td>
                       <td><?php echo $data1['email'];?></td>
                       <td><?php echo $data1['keluhan'];?></td>
                       <td>
                         <?php
                       if($data1["status"]=='Read'){
                         ?>
                         <span class="label label-lg label-success"><?php echo $data1['status'];?></span>
                         <?php
                       }else if($data1["status"]=='Unread'){
                         ?>
                         <span class="label label-lg label-warning"><?php echo $data1['status'];?></span>
                         <?php
                       }
                         ?>
                       </td>
                       <td><?php echo $data1['pesan_baru'];?></td>
                       <td><a href="../detail_keluhan.php?id=<?php echo $data1['id_ticket'];?>" class="btn btn-xs btn-primary btn-sm">Detail</a> <a href="hapus.php?id_produk=<?php echo $data1['id_ticket'];?>&status=<?php echo @$_GET["status"];?>&halaman=<?php echo @$_GET["halaman"];?>" class="btn btn-xs btn-danger btn-sm">Delete</a></td>
                   </tr>
                   <?php
                   $no++;
                     }
                   ?>
               </tbody>
             </table>
        <div class="col-md-12">
        <div class="blog-pagination">
          <center>
          <ul class="pagination">
            <?php
            // $paging2 = mysqli_query($connect,"SELECT * FROM keluhan WHERE kategori = 'produk' AND pesan_baru = 'Ada' AND deleted_at IS NULL ORDER BY created_at DESC");
            $paging2 = "SELECT * FROM keluhan WHERE kategori = :a AND pesan_baru = :b AND deleted_at IS :c ORDER BY created_at DESC";
            $stmt = $connect->prepare($paging2);
            $stmt->execute(['a' => 'Produk', 'b' => 'Ada', 'c' => NULLs]);
            $jmldata = $stmt->rowCount();
            // $jmldata = mysqli_num_rows($paging2);
            $jmlhalaman = ceil($jmldata/$batas);
            for($i=1; $i<=$jmlhalaman; $i++){
              if($i != $halaman){
                ?>
                <li><a href="?status=pesan_baru&halaman=<?php echo $i?>" class="btn-xs btn-primary"><?php echo $i?></a></li>
                <?php
              }else{
                ?>
                <li class="active"><a style="padding:4px 10px" href="?kategori=all&halaman=<?php echo $i?>"><?php echo $i?></a></li>
                <?php
              }
            }
            ?>
          </ul>
        </center>
        </div>
      </div>
    </div>
  </div>
  <?php
}
?>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <br/><br/><br/>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../js/bootstrap.min.js"></script>

</body>

</html>
