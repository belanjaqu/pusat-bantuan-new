<?php

    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "../proses/koneksi.php";

    $excel = new PHPExcel();

    $excel->getProperties()->setCreator('Adit Putra')             
    					->setLastModifiedBy('Adit Putra')             
    					->setTitle("Daftar Keluhan Fitur")             
    					->setSubject("Care BelanjaQu")             
    					->setDescription("Laporan Semua Data Keluhan Fitur")             
    					->setKeywords("Daftar Keluhan Fitur");

    $style_col = array(
    	'font' => array('bold' => true), 
    	'alignment' => array(
    		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 
    		'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,  
    		'borders' => array(
    			'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
    			'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
    			'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
    			'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)
    		)
    	)
    );

    $style_row = array(
    	'alignment' => array(
    		'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER, 
    		'borders' => array(
    			'top' => array(
    				'style' => PHPExcel_Style_Border::BORDER_THIN
    			), 
    			'right' => array(
    				'style'  => PHPExcel_Style_Border::BORDER_THIN
    			), 
    			'bottom' => array(
    				'style' => PHPExcel_Style_Border::BORDER_THIN
    			), 
    			'left' => array(
    				'style'  => PHPExcel_Style_Border::BORDER_THIN
    			)
    		)
    	)
    );

    $excel->setActiveSheetIndex(0)->setCellValue('A1', "DAFTAR KELUHAN FITUR"); 

    $excel->getActiveSheet()->mergeCells('A1:I1'); 
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); 
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); 
    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
    $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); 
    $excel->setActiveSheetIndex(0)->setCellValue('B3', "TICKET ID");
    $excel->setActiveSheetIndex(0)->setCellValue('C3', "WAKTU");
    $excel->setActiveSheetIndex(0)->setCellValue('D3', "PENGIRIM"); 
    $excel->setActiveSheetIndex(0)->setCellValue('E3', "EMAIL"); 
    $excel->setActiveSheetIndex(0)->setCellValue('F3', "KATEGORI");
    $excel->setActiveSheetIndex(0)->setCellValue('G3', "STATUS");
    $excel->setActiveSheetIndex(0)->setCellValue('H3', "KELUHAN"); 
    $excel->setActiveSheetIndex(0)->setCellValue('I3', "BALASAN"); 
    $excel->setActiveSheetIndex(0)->setCellValue('J3', "HAK"); 
    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
    $excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
    $excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);

    $query = "SELECT a.id_ticket, a.waktu, a.pengirim, a.email, a.kategori, a.status, a.keluhan, GROUP_CONCAT(b.konten SEPARATOR ';') AS mess, b.hak AS hak FROM keluhan a INNER JOIN message b ON b.keluhan_id = a.id_ticket WHERE a.kategori = :a GROUP BY a.id_ticket ORDER BY created_at DESC";
    $stmt = $connect->prepare($query);
    $stmt->execute(['a' => 'Fitur Belanjaqu']);
    $no = 1; 
    $numrow = 4;
    $time = date("d-m-Y H:i:s");

    while($row = $stmt->fetch()){
        if ($row['status'] == 'Read') {
            $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);  
            $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $row['id_ticket']);
            $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $row['waktu']); 
            $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $row['pengirim']);  
            $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $row['email']);  
            $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $row['kategori']);
            $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $row['status']);  
            $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $row['keluhan']);  
            $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $row['mess']);
            $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $row['hak']);  
            // $excel->setActiveSheetIndex(0)->setCellValueExplicit('J'.$numrow, $data['telp'], PHPExcel_Cell_DataType::TYPE_STRING);    

            $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);  
            $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);  
            $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);  
            $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);  
            $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);  
            $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);    
            $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);    
            $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);    
            $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);    
            $excel->getActiveSheet()->getRowDimension($numrow)->setRowHeight(20);    
            $no++; 
            $numrow++;
        }
    }

    $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); 
    $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    $excel->getActiveSheet(0)->setTitle("Laporan Daftar Keluhan Fitur");$excel->setActiveSheetIndex(0);

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Daftar Keluhan Fitur ' . $time . '.xlsx"'); 
    header('Cache-Control: max-age=0');

    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $write->save('php://output');
?>