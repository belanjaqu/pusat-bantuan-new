<style type="text/css">
@media screen and (max-width: 767px){.navbar-collapse {background: #3d3d3d;}}
</style>

<?php
echo '<!DOCTYPE html>',
'<html lang="en">';

include "bagian/head.php";

echo '<body>';

?>
    <div id="wrapper">

      <?php
          include "bagian/header.php";
      ?>

      <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
              <li class="active">
                  <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
              </li>
              <li>
                  <a style="cursor:pointer;" id="flip"><i class="fa fa-fw fa-table"></i> Keluhan <i class="fa fa-fw fa-caret-down"></i></a>
                  <ul id="panel" style="display: none;">
                    <li>
                        <a href="keluhan/all.php?status=all">All</a>
                    </li>
                    <li>
                        <a href="keluhan/pembayaran.php?status=all">Pembayaran</a>
                    </li>
                    <li>
                        <a href="keluhan/pengiriman.php?status=all">Pengiriman</a>
                    </li>
                    <li>
                        <a href="keluhan/produk.php?status=all">Produk</a>
                    </li>
                    <li>
                        <a href="keluhan/Pembelian.php?status=all">Pembelian</a>
                    </li>
                    <li>
                        <a href="keluhan/akun.php?status=all">Akun</a>
                    </li>
                    <li>
                        <a href="keluhan/fitur.php?status=all">Fitur Belanjaqu</a>
                    </li>
                  </ul>
              </li>
                            <li>
                                <a href="admin.php"><i class="fa fa-fw fa-user"></i> Admin</a>
                            </li>

          </ul>
      </div>
      </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12"><br/>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Dashboard
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                  <div class="col-lg-2"></div>
                  <div class="col-lg-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Terbaru & Belum Dibaca</h3>
                            </div>
                            <div class="panel-body">
                                <div class="list-group">
                                  <?php
                                         // $query = mysqli_query($connect, "SELECT * FROM keluhan WHERE status ='Unread' AND deleted_at IS NULL ORDER BY waktu DESC LIMIT 10");
                                         $query = "SELECT * FROM keluhan WHERE status ='Unread' AND deleted_at IS NULL ORDER BY waktu DESC LIMIT 10";
                                         $stmt = $connect->prepare($query);
                                         $stmt->execute();
                                         $no1 = 1;
                                         while($data1 = $stmt->fetch()){
                                      ?>
                                    <a href="detail_keluhan.php?id=<?php echo $data1['id_ticket'];?>" class="list-group-item">
                                    <?php echo $data1['keluhan'];?>
                                    <div>
                                        <span class="label label-primary"><?php echo $data1['pengirim'];?></span>
                                        <span class="label label-success"><?php echo $data1['waktu'];?></span>
                                        <span class="label label-default"><?php echo $data1['kategori'];?></span>
                                    </div>
                                    </a>
                                    <?php
                                      }
                                    ?>
                                </div>
                                <div class="text-right">
                                    <a href="keluhan/all.php?status=unread">Tampilkan Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
            <!-- /.container-fluid -->
            <br/><br/><br/>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

    <script>
      $("#flip").click(function(){
      $("#panel").slideToggle("slow");
      });
    </script>

</body>

</html>
