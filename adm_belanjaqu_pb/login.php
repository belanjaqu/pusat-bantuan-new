<!DOCTYPE html>
<html>
  <head>
      <title>Pusat Bantuan BelanjaQu</title>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="../css/materialize.min.css"  media="screen,projection"/>
	    <link rel="shortcut icon" href="../images/favicon.png"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <style type="text/css">
        @media screen and (max-width: 750px){
          .form_login {width:95%;padding:13px 15px}
          .logo_login{width:80%}
          }
      </style>

  </head>
  <?php

  session_start();
  date_default_timezone_set('Asia/Jakarta');
  require "../proses/koneksi.php";

    if(isset($_POST['masuk'])){
    $email= $_POST['email'];
    $katasandi2 = $_POST['password'];
    //$katasandi2 = md5($katasandi);
    // $a = mysqli_query($connect, "SELECT *FROM admin WHERE email='$email' AND password='$katasandi2'");
    $query = "SELECT *FROM admin WHERE email = :a AND deleted_at IS :b AND password= :c";
    $stmt = $connect->prepare($query);
    $stmt->execute(['a' => $email, 'b' => NULL, 'c' => $katasandi2]);
    // $b = mysqli_fetch_array($a,MYSQLI_ASSOC);
    $b = $stmt->fetch();
    $num_rows = $stmt->rowCount();
      if ($num_rows == 1){
        $_SESSION['email']=$email;
        $_SESSION['hak']=$b['hak'];

        header("location:index.php");
      }else{
            echo "<script>alert('Login gagal ! Silahkan periksa kembali Email & Password.')</script>";
      }
    }
  ?>

  <body>
      <div class="container">
        <form class="col s12 form_login" method="post" enctype="multipart/form-data">
          <img src="../images/Logo_berwarna.png" class="logo_login" />
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">account_circle</i>
              <input id="email" type="email" class="validate" name="email">
              <label for="email" data-error="Format email salah!" data-success="">Email</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">lock</i>
              <input id="password" type="password" class="validate" name="password">
              <label for="password">Password</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <button type="submit" class="waves-effect waves-light btn-large" style="width:100%;" name="masuk"><b>MASUK</b></button>
            </div>
          </div>
        </form>
      </div>

      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="../js/materialize.min.js"></script>
  </body>
</html>
