<?php
echo '<!DOCTYPE html>',
'<html lang="en">';

include "bagian/head.php";

echo '<body>';

	if(isset($_POST['tambah_admin'])){

		$fileName = $_FILES['foto']['name'];
		$fileSize = $_FILES['foto']['size'];
		$fileError = $_FILES['foto']['error'];
	 	if($fileSize > 0 || $fileError == 0){
				$move = move_uploaded_file($_FILES['foto']['tmp_name'], '../images/'.$fileName);
		 	}

 $nama = ($_POST['nama']);
 $password = md5($_POST['password']);
 $email = ($_POST['email']);
 $jabatan = ($_POST['jabatan']);
 $jk = ($_POST['jk']);
 $hak = ($_POST['hak']);
 $foto = $fileName;

 $sql_query = "INSERT INTO admin VALUES('', :a, :b, :c, :d, :e, :f, :g)";
 $stmt = $connect->prepare($query);                                  
 $stmt->bindParam(':a', $nama);       
 $stmt->bindParam(':b', $password);      
 $stmt->bindParam(':c', $email);
 $stmt->bindParam(':d', $jabatan);      
 $stmt->bindParam(':e', $jk);      
 $stmt->bindParam(':f', $hak);
 $stmt->bindParam(':g', $foto);          
 $stmt->execute(); 

 if($stmt->execute()){
	 echo "<script>alert('Berhasil')</script>";
	 header("location:admin.php");
 }else{
	 echo "<script>alert('Gagal')</script>";
 }
}

?>
    <div id="wrapper">

      <?php
          include "bagian/header.php";
      ?>

      <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
              <li>
                  <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
              </li>
							<li>
			            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-table"></i> Keluhan <i class="fa fa-fw fa-caret-down"></i></a>
			            <ul id="demo" class="collapse">
										<li>
 											 <a href="keluhan/all.php?status=all">All</a>
 									 </li>
 									 <li>
 											 <a href="keluhan/pembayaran.php?status=all">Pembayaran</a>
 									 </li>
 									 <li>
 											 <a href="keluhan/pengiriman.php?status=all">Pengiriman</a>
 									 </li>
 									 <li>
 											 <a href="keluhan/produk.php?status=all">Produk</a>
 									 </li>
 									 <li>
 											 <a href="keluhan/Pembelian.php?status=all">Pembelian</a>
 									 </li>
 									 <li>
 											 <a href="keluhan/akun.php?status=all">Akun</a>
 									 </li>
 									 <li>
 											 <a href="keluhan/fitur.php?status=all">Fitur Belanjaqu</a>
 									 </li>
			            </ul>
			        </li>


              <li>
                  <a href="admin.php"><i class="fa fa-fw fa-user"></i> Admin</a>
              </li>
          </ul>
      </div>
      </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1>
                            Tambah Admin
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-user"></i> Admin
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Tambah Admin
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-6">

                        <form role="form" method="post" enctype="multipart/form-data">

                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" class="form-control" name="nama">
                            </div>

                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password">
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" placeholder="belanja@belanjaqu.co.id" name="email">
                            </div>

                            <div class="form-group">
                                <label>Jabatan</label>
                                <input class="form-control" name="jabatan">
                            </div>

                            <div class="form-group">
                                <label>File input</label>
                                <input type="file" name="foto">
                            </div>

                            <div class="form-group">
                                <label>Jenis Kelamin</label>
                                <select class="form-control" name="jk">
                                    <option value="Laki-laki">Laki-laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Hak Akses </label><br>
                                <label class="radio-inline">
                                    <input type="radio"  name="hak" id="optionsRadiosInline1" value="Admin Master" checked> Admin Master
                                </label>
                                <label class="radio-inline">
                                    <input type="radio"  name="hak" id="optionsRadiosInline2" value="Admin"> Admin
                                </label>
                            </div>

                            <hr>

                            <button type="submit" class="btn btn-success pull-right" name="tambah_admin">Kirim</button>

                        </form>

                    </div>
                </div>
                <!-- /.row -->
								<br />
								<br />
								<br />

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
