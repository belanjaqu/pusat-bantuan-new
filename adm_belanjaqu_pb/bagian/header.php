
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Pusat Bantuan BelanjaQu</a>
    </div>

    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $admin['nama'];?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="#"><i class="fa fa-fw fa-envelope"></i> <?php echo $admin['email'];?></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-black-tie"></i> <?php echo $admin['jabatan'];?></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-unlock-alt"></i> <?php echo $admin['hak'];?></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="keluar.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
