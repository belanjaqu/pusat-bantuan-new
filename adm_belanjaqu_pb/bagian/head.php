<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ADM BelanjaQu Pusat Bantuan</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <link href="css/gaya.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="../images/favicon.png"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<?php
session_start();
date_default_timezone_set('Asia/Jakarta');
require "../proses/koneksi.php";

  if(empty($_SESSION['email'])){
	   header("location:login.php");
	}
  $email = $_SESSION['email'];
	// $query = mysqli_query($connect, "SELECT * FROM admin where email = '$email' ");
	// $admin = mysqli_fetch_assoc($query);
    $query = "SELECT * FROM admin where email = :a";
    $stmt = $connect->prepare($query);
    $stmt->execute(['a' => $email]);
    $admin = $stmt->fetch();

?>
