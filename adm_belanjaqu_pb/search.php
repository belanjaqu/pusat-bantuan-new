<style type="text/css">
@media screen and (max-width: 767px){.navbar-collapse {background: #3d3d3d;}}
.ikon_pilari{padding:2px 3px 4px 3px}.eusi-tabel{font-size:13px}
</style>

<?php
echo '<!DOCTYPE html>',
'<html lang="en">';

include "bagian/head.php";

echo '<body>';

if(!empty($_GET['cari'])){
	$isi = $_GET['cari'];
	$cari = str_replace("-", " ", "$isi");
}else{

}

?>
    <div id="wrapper">

      <?php
          include "bagian/header.php";
      ?>

      <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
              <li>
                  <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
              </li>
              <li class="active">
                  <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-table"></i> Keluhan <i class="fa fa-fw fa-caret-down"></i></a>
                  <ul id="demo" class="collapse">
										<li>
												<a href="keluhan/all.php?status=all">All</a>
										</li>
										<li>
												<a href="keluhan/pembayaran.php?status=all">Pembayaran</a>
										</li>
										<li>
												<a href="keluhan/pengiriman.php?status=all">Pengiriman</a>
										</li>
										<li>
												<a href="keluhan/produk.php?status=all">Produk</a>
										</li>
										<li>
												<a href="keluhan/Pembelian.php?status=all">Pembelian</a>
										</li>
										<li>
												<a href="keluhan/akun.php?status=all">Akun</a>
										</li>
										<li>
												<a href="keluhan/fitur.php?status=all">Fitur Belanjaqu</a>
										</li>
                  </ul>
              </li>
                            <li>
                                <a href="admin.php"><i class="fa fa-fw fa-user"></i> Admin</a>
                            </li>
          </ul>
      </div>
      </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12"><br/>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> Keluhan
                            </li>
                            <li class="active">
                                <i class="fa fa-search"></i> Serach
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row"><br/>
				          <div class="col-lg-12">
				            <form role="search" method="GET">
				              <div class="input-group">
				                  <?php
				                    if(!empty($_GET['cari'])){
				                      $y = $_GET['cari'];
				                      echo '<input type="text" class="form-control" placeholder="Pencarian..." name="cari" required />';
				                    }else{
				                      echo '<input type="text" class="form-control" placeholder="Pencarian..." name="cari" required />';
				                    }
				                    ?>
				                <div class="input-group-btn">
				                  <button class="btn btn-default" type="submit">
				                    <i class="glyphicon glyphicon-search ikon_pilari"></i>
				                  </button>
				                </div>
				              </div>
				            </form>
										<h5>Hasil pencarian untuk <b><?php echo $cari;?></b></h5>
				          </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12"><br>
                        <div class="table-responsive table-bordered">
                          <?php
                          $batas = 10;
                          $halaman = @$_GET['halaman'];
                          if(empty($halaman)){
                             $posisi = 0;
                             $halaman = 1;
                            }else{
                               $posisi = ($halaman - 1) * $batas;
                            }

                                // $query = mysqli_query($connect, "SELECT * FROM keluhan WHERE pengirim LIKE '%$cari%' OR email LIKE '%$cari%' OR id_ticket LIKE '%$cari%' AND deleted_at IS NULL ORDER BY created_at DESC");
                                $query = "SELECT * FROM keluhan WHERE pengirim LIKE :a OR email LIKE :b OR id_ticket LIKE :c AND deleted_at IS NULL ORDER BY created_at DESC";
                                $stmt = $connect->prepare($query);
                                $stmt->bindValue(':a','%'.$cari.'%');
                                $stmt->bindValue(':b','%'.$cari.'%');
                                $stmt->bindValue(':c','%'.$cari.'%');
                                $stmt->execute();
                                $total = $stmt->rowCount();
                                // $total = mysqli_num_rows($query);
                                ?>
                            <table class="table table-hover table-striped eusi-tabel">
                                <thead>
                                    <tr>
                                        <th>ID Ticket</th>
                                        <th>Waktu</th>
                                        <th>Pengirim</th>
                                        <th>Email</th>
                                        <th>Keluhan</th>
                                        <th>Kategori</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                         $no = 1+$posisi;
                                         while($data1 = $stmt->fetch()){
                                      ?>
                                    <tr>
                                        <td><?php echo $data1['id_ticket'];?></td>
                                        <td><?php echo $data1['waktu'];?></td>
                                        <td><?php echo $data1['pengirim'];?></td>
                                        <td><?php echo $data1['email'];?></td>
                                        <td><?php echo $data1['keluhan'];?></td>
                                        <td><?php echo $data1['kategori'];?></td>
                                        <td>
                                          <?php
                                        if($data1["status"]=='Read'){
                                          ?>
                                          <span class="label label-lg label-success"><?php echo $data1['status'];?></span>
                                          <?php
                                        }else if($data1["status"]=='Unread'){
                                          ?>
                                          <span class="label label-lg label-warning"><?php echo $data1['status'];?></span>
                                          <?php
                                        }
                                          ?>
                                        </td>
                                        <td><a href="detail_keluhan.php?id=<?php echo $data1['id_ticket'];?>" class="btn btn-xs btn-primary btn-sm">Detail</a> <a href="keluhan/hapus.php?id=<?php echo $data1['id_ticket'];?>" class="btn btn-xs btn-danger btn-sm">Hapus</a></td>
                                    </tr>
                                    <?php
                                    $no++;
                                      }
                                    ?>
                                </tbody>
                            </table>
                            <div class="col-md-12">
                            <div class="blog-pagination">
                              <center>
                              <ul class="pagination">
                                <?php
                                // $paging2 = mysqli_query($connect,"select * from keluhan");
                                $paging2 = "SELECT * FROM keluhan";
                                $stmt = $connect->prepare($paging2);
                                $stmt->execute();
                                $jmldata = $stmt->rowCount();
                                // $jmldata = mysqli_num_rows($paging2);
                                $jmlhalaman = ceil($jmldata/$batas);
                                for($i=1; $i<=$jmlhalaman; $i++){
                                  if($i != $halaman){
                                    ?>
                                    <li><a href="?kategori=all&halaman=<?php echo $i?>" class="btn-xs btn-primary"><?php echo $i?></a></li>
                                    <?php
                                  }else{
                                    ?>
                                    <li class="active"><a style="padding:4px 10px" href="?kategori=all&halaman=<?php echo $i?>"><?php echo $i?></a></li>
                                    <?php
                                  }
                                }
                                ?>
                              </ul>
                            </center>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            <br/><br/><br/>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

</body>

</html>
