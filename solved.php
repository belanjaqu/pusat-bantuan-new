        <?php include 'design/header.php' ?>
        <?php session_start(); ?>
        <div class="main-content">
          <div class="grid-container">
            <div class="tac">
              <h5>Terima Kasih,&nbsp;<strong><?php echo $_SESSION['nama_pengirim'] ?></strong></h5>
              <p class="tac gray">Terima kasih telah menghubungi, senang bisa membantu Anda.</p>
            </div>
            <div class="case-container grid-x">
              <div class="list-case small-12 large-6">
                <div class="tac">
                  <img src="images/solved.png" alt="Solved">
                </div>
                <div class="help-more">
                  <strong>Apakah kamu punya pertanyaan lain ?</strong>
                  <div class="grid-x grid-margin-x">
                    <div class="small-12 large-6 cell">
                      <a href="https://belanjaqu.co.id" class="button expanded">Tidak, kembali belanja</a>
                    </div>
                    <div class="small-12 large-6 cell">
                      <a href="index.php" class="button expanded">Ya, saya punya pertanyaan lain</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php include 'design/footer.php' ?>