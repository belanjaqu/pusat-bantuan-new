<?php
  if (isset($_SESSION)) {
    session_destroy();
  }
  session_start();
  date_default_timezone_set('Asia/Jakarta');
  require "proses/koneksi.php";
  $fdate = date('Y-m-d H:i:s');

  if(isset($_POST['kirim'])){
    // $carikode = mysqli_query($connect, "SELECT MAX(id_ticket) FROM keluhan WHERE deleted_at IS NULL") or die (mysql_error());
    //$carikode = "SELECT MAX(id_ticket) FROM keluhan";
    $carikode = "SELECT MAX(CAST(SUBSTRING(id_ticket, 2, length(id_ticket)) AS UNSIGNED)) FROM keluhan";
    $stmt = $connect->prepare($carikode);
    $stmt->execute();
    // menjadikannya array
    $datakode = $stmt->fetch();
    // $datakode = mysqli_fetch_array($carikode);
    // jika $datakode
    if ($datakode) {
      //$nilaikode = substr($datakode[0], 1);
      // menjadikan $nilaikode ( int )
      //$kode = (int) $nilaikode;
      $kode = (int)$datakode[0];
      // setiap $kode di tambah 1
      $kode = $kode + 1;
      //$kode_otomatis = "T".str_pad($kode, 4, "0", STR_PAD_LEFT);
      $kode_otomatis = "T" . (string)$kode;
    } else {
      $kode_otomatis = "T0001";
    }
    $tgl= date('j');
    /* script menentukan bulan */
    $array_bln = array(1=>"Januari","Februari","Maret", "April", "Mei","Juni","Juli","Agustus","September","Oktober", "November","Desember");
    $bln = $array_bln[date('n')];
    /* script menentukan tahun */
    $thn = date('Y');
    /* script perintah keluaran*/
    $jam = date('H:i');
    /* script perintah keluaran*/
    if(!empty($_POST['no_invoice'])){
      $id_ticket = $kode_otomatis;
      $pengirim = ($_POST['nama_pengirim']);
      $email = ($_POST['email_pengirim']);
      $keluhan = ($_POST['keluhan']);
      $kategori = ($_POST['kategori']);
      $no_invoice = ($_POST['no_invoice']);
      $tgl_info =   $jam . ", " . $tgl . " " . $bln . " " . $thn;
      $secret_key = '6LfIVzgUAAAAAE8nCpLbU_BOqyao48Q464PYGMfv';
      $captcha = isset($_POST['g-recaptcha-response']) ? $_POST['g-recaptcha-response']:'';
      $sql_query = "INSERT INTO keluhan VALUES(:a, :b, :c, :d, :e, :f, 'Unread', :h, 'Ada', :j, NULL)";
      $stmt = $connect->prepare($sql_query);                              
      $stmt->bindParam(':a', $kode_otomatis);
      $stmt->bindParam(':b', $pengirim);
      $stmt->bindParam(':c', $email);
      $stmt->bindParam(':d', $kategori);
      $stmt->bindParam(':e', $keluhan);
      $stmt->bindParam(':f', $tgl_info);
      $stmt->bindParam(':h', $no_invoice);
      $stmt->bindParam(':j', $fdate);

      if ($captcha != '') {
        if($stmt->execute()){
          // echo "<script>alert('Keluhan terkirim, ID Tiket Anda : ".$kode_otomatis.". Buka email untuk melihat balasan.')</script>";
          $_SESSION = $_POST;
          $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secret_key) . '&response=' . $captcha;
          $recaptcha = file_get_contents($url);
          $recaptcha = json_decode($recaptcha, true);
          if (!$recaptcha['success']) {
            echo "<script>alert('Captcha Error')</script>";
          }
          header("location: solved.php");
        }else{
          echo "<script>alert('Gagal')</script>";
          // echo var_dump($stmt->execute());
        }
      } else {
        echo "<script>alert('Harap Masukkan Captcha!')</script>";
      }
    } else if(empty($_POST['no_invoice'])){
      $id_ticket = $kode_otomatis;
      $pengirim = ($_POST['nama_pengirim']);
      $email = ($_POST['email_pengirim']);
      $keluhan = ($_POST['keluhan']);
      $kategori = ($_POST['kategori']);
      $tgl_info =   $jam . ", " . $tgl . " " . $bln . " " . $thn;
      $secret_key = '6LfIVzgUAAAAAE8nCpLbU_BOqyao48Q464PYGMfv';
      $captcha = isset($_POST['g-recaptcha-response']) ? $_POST['g-recaptcha-response']:'';

      $sql_query = "INSERT INTO keluhan VALUES(:a, :b, :c, :d, :e, :f, 'Unread', '', 'Tidak', :i, NULL)";
      $stmt = $connect->prepare($sql_query);                                  
      $stmt->bindParam(':a', $kode_otomatis);       
      $stmt->bindParam(':b', $pengirim);      
      $stmt->bindParam(':c', $email);      
      $stmt->bindParam(':d', $kategori);      
      $stmt->bindParam(':e', $keluhan);      
      $stmt->bindParam(':f', $tgl_info);      
      $stmt->bindParam(':i', $fdate);      

      if ($captcha != '') {
        if($stmt->execute()){
          // echo "<script>alert('Keluhan terkirim, ID Tiket Anda : ".$kode_otomatis.". Buka email untuk melihat balasan.')</script>";
          $_SESSION = $_POST;
          $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secret_key) . '&response=' . $captcha;
          $recaptcha = file_get_contents($url);
          $recaptcha = json_decode($recaptcha, true);
          if (!$recaptcha['success']) {
            echo "<script>alert('Captcha Error')</script>";
          }
          header("location: solved.php");
        }else{
          echo "<script>alert('Coba Lagi')</script>";
        // echo var_dump($stmt->execute());
        }
      } else {
        echo "<script>alert('Harap Masukkan Captcha!')</script>";
      }
    }
  }
?>