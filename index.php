        <?php include 'design/header.php' ?>
        <div class="main-content">
          <div class="grid-container">
            <div class="tac">
              <h5><strong>Pusat Bantuan BelanjaQu</strong></h5>
              <p class="gray">Terima kasih telah menghubungi kami. Silakan pilih bantuan yang kamu butuhkan :</p>
            </div>
            <div class="list-content grid-x grid-margin-x">
              <div class="item small-12 large-4 cell">
                <div class="card">
                  <a href="form_akun.php">
                    <div class="grid-x">
                      <div class="img-box">
                        <img src="images/account.png" alt="Item Icon">
                      </div>
                      <div class="auto cell">
                        <div class="desc-box">
                          <div class="title">Akun &amp; Info Personal</div>
                          <div class="desc">Seputar perihal akun dan datanya, tidak bisa login, chat akun yang ditolak sistem, dan lainnya.</div>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
              <div class="item small-12 large-4 cell">
                <div class="card">
                  <a href="form_transaksi.php">
                    <div class="grid-x">
                      <div class="img-box">
                        <img src="images/buy.png" alt="Item Icon">
                      </div>
                      <div class="auto cell">
                        <div class="desc-box">
                          <div class="title">Transaksi Pembelian</div>
                          <div class="desc">Konfirmasi pembelian, produk yang dibeli tidak sesuai, retur produk.</div>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
              <div class="item small-12 large-4 cell">
                <div class="card">
                  <a href="form_penjualan.php">
                    <div class="grid-x">
                      <div class="img-box">
                        <img src="images/sell.png" alt="Item Icon">
                      </div>
                      <div class="auto cell">
                        <div class="desc-box">
                          <div class="title">Penjualan</div>
                          <div class="desc">Lupa konfirmasi pengiriman, salah input resi, perbedaan ongkir, ubah kurir.</div>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
              <div class="item small-12 large-4 cell">
                <div class="card">
                  <a href="form_pembayaran.php">
                    <div class="grid-x">
                      <div class="img-box">
                        <img src="images/transaction.png" alt="Item Icon">
                      </div>
                      <div class="auto cell">
                        <div class="desc-box">
                          <div class="title">Pembayaran</div>
                          <div class="desc">Konfirmasi pembayaran, transfer tak sesuai tagihan, pembayaran tidak masuk.</div>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
              <div class="item small-12 large-4 cell">
                <div class="card">
                  <a href="form_produk.php">
                    <div class="grid-x">
                      <div class="img-box">
                        <img src="images/product.png" alt="Item Icon">
                      </div>
                      <div class="auto cell">
                        <div class="desc-box">
                          <div class="title">Produk</div>
                          <div class="desc">Produk tidak tampil, tidak lolos moderasi, stok produk, dan lainnya.</div>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
              <div class="item small-12 large-4 cell">
                <div class="card">
                  <a href="form_fitur.php">
                    <div class="grid-x">
                      <div class="img-box">
                        <img src="images/featured.png" alt="Item Icon">
                      </div>
                      <div class="auto cell">
                        <div class="desc-box">
                          <div class="title">Fitur BelanjaQu</div>
                          <div class="desc">Pertanyaan umum seputar BelanjaQu.</div>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php include 'design/footer.php' ?>