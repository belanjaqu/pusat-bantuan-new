<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <!-- <meta name="viewport" content="width=720"> -->
    <title>Hubungi BelanjaQu | BelanjaQu</title>
    <link rel="stylesheet" href="css/app.css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
  </head>
  <body>
    <div class="off-canvas-wrapper">
      <div class="content-wrapper">
        <header class="header-container">
          <div class="header">
            <section class="main-header"> 
              <div class="grid-container">
                <div class="grid-x">
                  <div class="small-12 cell">
                    <div class="logo-container">
                      <a href="index.php">
                        <img src="images/logogram.png" alt="Logo BelanjaQu">
                        <!-- <img src="../images/logogram.png" alt="Logo BelanjaQu"> -->
                      </a>
                    </div>
                    <ul class="menu fr">
                      <li><a href="https://belanjaqu.co.id">Belanja</a></li>
                      <li><a href="https://belanjaqu.co.id/home/pusat-penganduan">Laporkan Penyalahgunaan</a></li>
                      <!-- <li><a href="" class="lgn-btn">Login</a></li> -->
                    </ul>
                    <div class="toggle-menu fr hidden" data-open="menuModal">
                      <svg class="ic-hamburger"><use xlink:href="images/icons.svg#ic-hamburger"></use></svg>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </header>