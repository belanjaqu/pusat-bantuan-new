        <?php include 'design/header.php' ?>
        <?php include 'exec_form.php' ?>
        <div class="main-content">
          <div class="grid-container">
            <div class="tac">
              <h5><strong>Silakan sampaikan masalah kamu lebih lanjut</strong></h5>
            </div>
            <div class="case-container grid-x">
              <form class="form-case small-12 large-6" method="post">
                <div class="fblock">
                  <label>Masalah</label>
                  <div class="card form-control">Penjualan</div>
                  <input type="hidden" name="kategori" class="form-control" value="Pengiriman">
                </div>
                <div class="fblock">
                  <label>Detail Masalah</label>
                  <textarea rows="4" name="keluhan" class="form-control" placeholder="Tuliskan detail permasalahan kamu" required=""></textarea>
                </div>
                <div class="fblock">
                  <label>Nomor Invoice</label>
                  <input type="text" name="no_invoice" class="form-control" placeholder="Masukan nomor invoice" required="">
                </div>
                <div class="fblock">
                  <label>Nama</label>
                  <input type="text" name="nama_pengirim" class="form-control" placeholder="Masukan nama kamu" required="">
                </div>
                <div class="fblock">
                  <label>Email</label>
                  <input type="text" name="email_pengirim" class="form-control" placeholder="Masukan email kamu" required="">
                </div>
                <div class="fblock">
                  <div class="g-recaptcha" data-sitekey="6LfIVzgUAAAAAO_G8tnXdsQZQsVwg6j4FQmiW-f7"></div>
                </div>
                <div class="fblock">
                  <button type="submit" class="button expanded" name="kirim">Kirim Sekarang</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <?php include 'design/footer.php' ?>