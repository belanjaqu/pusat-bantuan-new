<!DOCTYPE html>
<html>
  <head>
      <title>Pusat Bantuan BelanjaQu</title>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link rel="stylesheet" type="text/css" href="../css/materialize.min.css"  media="screen,projection" />
      <link rel="stylesheet" type="text/css" href="../css/chat_page.css" />
      <link rel="shortcut icon" href="../images/favicon.png"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>

<?php

session_start();
date_default_timezone_set('Asia/Jakarta');
require "../proses/koneksi.php";

if(empty($_GET['url']) && empty($_GET['urll']) && empty($_GET['urlll'])){
   header("location:../index.php");
}

$id = base64_decode($_GET['url']);
$idd = $id;
$arr_kalimat = explode (" ",$idd);
$hasil = $arr_kalimat[0];
// $query = mysqli_query($connect, "SELECT * FROM keluhan WHERE id_ticket = '$hasil' AND deleted_at IS NULL");
$query = "SELECT * FROM keluhan WHERE id_ticket = :a AND deleted_at IS :b";
$stmt = $connect->prepare($query);
$stmt->execute(['a' => $hasil, 'b' => NULL]);
// $data = mysqli_fetch_assoc($query);
$data = $stmt->fetch();
$fdate = date('Y-m-d H:i:s');
if(isset($_POST['kirim_balasan'])){
  $carikode = "SELECT MAX(id_ticket) FROM keluhan";
  $stmt = $connect->prepare($carikode);
  $stmt->execute();
  // $carikode = mysqli_query($connect, "SELECT MAX(id_ticket) FROM keluhan WHERE deleted_at IS NULL") or die (mysql_error());
  // menjadikannya array
  $datakode = $stmt->fetch();
  // jika $datakode
  if ($datakode) {
   $nilaikode = substr($datakode[0], 1);
   // menjadikan $nilaikode ( int )
   $kode = (int) $nilaikode;
   // setiap $kode di tambah 1
   $kode = $kode + 1;
   $kode_otomatis = "C".str_pad($kode, 4, "0", STR_PAD_LEFT);
  } else {
   $kode_otomatis = "T0001";
  }
$tgl= date('j');
/* script menentukan bulan */
$array_bln = array(1=>"Januari","Februari","Maret", "April", "Mei","Juni","Juli","Agustus","September","Oktober", "November","Desember");
$bln = $array_bln[date('n')];
/* script menentukan tahun */
$thn = date('Y');
/* script perintah keluaran*/
$jam = date('H:i');
/* script perintah keluaran*/

$keluhan_id = $data['id_ticket'];
$parent_id = $kode_otomatis;
$balas = ($_POST['balas']);
$tgl_info =   $jam . ", " . $tgl . " " . $bln . " " . $thn;
$timer = date('Y-m-d H:i:s');

// $sql_query = "INSERT INTO message VALUES('$keluhan_id', '$parent_id', '$balas', '$tgl_info', 'User', '$fdate');";
$sql_query = "INSERT INTO message VALUES(:a, :b, :c, :d, 'User', :e)";
    $stmt = $connect->prepare($sql_query);                                  
    $stmt->bindParam(':a', $keluhan_id);       
    $stmt->bindParam(':b', $parent_id);      
    $stmt->bindParam(':c', $balas);      
    $stmt->bindParam(':d', $tgl_info);      
    $stmt->bindParam(':e', $timer);      

         if($stmt->execute()){
            echo "<script>alert('Pesan terkirim..')</script>";
          }else{
             echo "<script>alert('Gagal')</script>";
          }

          $no = $data['id_ticket'];
          // $abc = mysqli_query($connect, "UPDATE keluhan SET pesan_baru = 'Ada', waktu = '$tgl_info', status = 'Unread', created_at = '$fdate' WHERE id_ticket = '$no'");
          $abc = "UPDATE keluhan SET pesan_baru = 'Ada', waktu = :b, status = 'Unread', created_at = :d WHERE id_ticket = :e";
          $stmt = $connect->prepare($abc);                                  
          $stmt->bindParam(':b', $tgl_info);      
          $stmt->bindParam(':d', $fdate);      
          $stmt->bindParam(':e', $no);      
          $stmt->execute();
        }

 ?>

  <body>

      <div class="data-diri">
        <table class="centered responsive-table">
          <thead>
            <tr>
                <th>Nama</th>
                <th>ID Tiket</th>
                <th>No Invoice</th>
                <th>Kategori</th>
                <th>Email</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td><?php echo $data['pengirim'];?></td>
              <td><?php echo $data['id_ticket'];?></td>
              <td><?php echo $data['no_invoice'];?></td>
              <td><?php echo $data['kategori'];?></td>
              <td><?php echo $data['email'];?></td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="container">
        <?php
        $keluhan_id2 = $id;
        // $query2 = mysqli_query($connect, "SELECT * FROM message WHERE keluhan_id = '$hasil' AND deleted_at IS NULL ORDER BY parent_id ASC LIMIT 100");
        $query2 = "SELECT * FROM message WHERE keluhan_id = :a ORDER BY parent_id ASC LIMIT 100";
        $stmt = $connect->prepare($query2);
        $stmt->execute(['a' => $hasil]);
        $total2 = $stmt->rowCount();
        // $total2 = mysqli_num_rows($query2);
        ?>
        <div class="row area-pesan">
          <div class="col l6 offset-l6">
            <ul class="right">
            <span class="namina"><?php echo $data['pengirim'];?> :</span>
              <li class="messages-user"><?php echo $data['keluhan'];?></li>
              <span class="waktosna"><?php echo $data['waktu'];?></span>
            </ul>
          </div>
          <?php
            while($data2 = $stmt->fetch()){
              if ($data2["hak"] == 'Admin'){
                ?>
                <div class="col s12 l12">
                  <ul class="left">
                  <span class="namina">Belanjaqu :</span>
                    <li class="messages-admin"><?php echo $data2['konten'];?></li>
                    <span class="waktosna"><?php echo $data2['waktu'];?></span>
                  </ul>
                </div>
                <?php
              } else {
                ?>
                <div class="col l6 offset-l6">
                  <ul class="right">
                  <span class="namina"><?php echo $data['pengirim'];?> :</span>
                    <li class="messages-user"><?php echo $data2['konten'];?></li>
                    <span class="waktosna"><?php echo $data2['waktu'];?></span>
                  </ul>
                </div>
                <?php
              }
              ?>
          <?php
            }
          ?>
        </div>

        <div id="box-input">
          <div class="row">
              <div class="col s12">
                <form method="POST">
                  <button type="submit" class="btn-floating waves-effect kirims" name="kirim_balasan" style="background:#757575;box-shadow:none"><i class="material-icons right">send</i></button>
                  <textarea class="pesan" placeholder="Tulis pesan anda..." name="balas"></textarea>
                </form>
              </div>
          </div>
        </div>
      </div>

      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="../js/materialize.min.js"></script>
      <script type="text/javascript">
      function h(e) {
        $(e).css({'height':'auto','overflow-y':'hidden'}).height(e.scrollHeight);
      }
      $('textarea').each(function () {
        h(this);
      }).on('input', function () {
        h(this);
      });
      </script>
  </body>
</html>
